import sys

from pylint.lint import Run


def run_lint(command):
    results = Run(command, exit=False)
    return results


if __name__ == '__main__':
    if run_lint(['bpm']).linter.stats['global_note'] >= 8:
        sys.exit(0)
    else:
        sys.exit(-1)
