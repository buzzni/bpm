import time

import requests

LOG_URL = 'http://localhost:5000/api/v1/logs/'
RULE_URL = 'http://localhost:5000/api/v1/rules/'
headers = {'Content-Type': 'application/json'}

log_cooltime = {
    "key_name": "cooltime",
    "value": 0,
    "host": "From Marco",
    "description": ""
}
rule_json = {
    "name": "test",
    "description": "rule for testing",
    "owner": "Marco",
    "email": "marco@buzzni.com",
    "key_name": "cooltime",
    "timeout": 10,
    "rule": {
        "type": "A",
        "boundary1": 50,
        "boundary2": 80
    },
    "channels": {
        "good": [],
        "caution": ["TEST4"],
        "emergency": [],
        "timeout": ["default"]
    }
}


def post_log(x):
    log_cooltime["value"] = int(x)
    info = ", ".join(["key_name=" + log_cooltime["key_name"],
                      "value=" + "\x1b[6;30;42m" + str(log_cooltime[
                                                           "value"]) + "\x1b[0m",
                      "host=" + log_cooltime["host"]])
    print("POST Logs: " + info)
    requests.post(LOG_URL, headers=headers, json=log_cooltime)


def post_rule():
    response = requests.post(RULE_URL, headers=headers, json=rule_json)
    print("POST 요청한 Rule 내용: ")
    print(response.text)


def get_rule():
    r = requests.get(RULE_URL, headers=headers)
    print("현재 BPM에 등록되어 있는 Rule들의 리스트: ")
    print(r.text)


def check_log_post_cooltime():
    # Every single 1 second, sending a post request
    # During 15 seconds
    for x in range(50, 90, 1):
        # print("Agent Value = {}".format(x))
        post_log(x)
        time.sleep(1)


def main():
    post_rule()
    get_rule()
    check_log_post_cooltime()


if __name__ == '__main__':
    main()
