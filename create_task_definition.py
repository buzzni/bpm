import json
import sys


def create_task_definition(database_url):
    task_definition = {
        "containerDefinitions": [
            {
                "logConfiguration": {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-group": "ecs-backoffice",
                        "awslogs-region": "ap-northeast-1",
                        "awslogs-stream-prefix": "bpm"
                    }
                },
                "cpu": 0,
                "environment": [
                    {
                        "name": "DATABASE_URL",
                        "value": database_url
                    },
                    {
                        "name": "BPM_SETTINGS",
                        "value": "production"
                    }
                ],
                "mountPoints": [],
                "memory": 200,
                "volumesFrom": [],
                "image": "101047223697.dkr.ecr.ap-northeast-1.amazonaws.com/buzzni/bpm/celery",
                "essential": True,
                "name": "bpm-celery"
            },
            {
                "logConfiguration": {
                    "logDriver": "awslogs",
                    "options": {
                        "awslogs-group": "ecs-backoffice",
                        "awslogs-region": "ap-northeast-1",
                        "awslogs-stream-prefix": "bpm"
                    }
                },
                "portMappings": [
                    {
                        "hostPort": 8087,
                        "protocol": "tcp",
                        "containerPort": 5000
                    }
                ],
                "cpu": 0,
                "environment": [
                    {
                        "name": "DATABASE_URL",
                        "value": database_url
                    },
                    {
                        "name": "BPM_SETTINGS",
                        "value": "production"
                    }
                ],
                "mountPoints": [],
                "memory": 500,
                "volumesFrom": [],
                "image": "101047223697.dkr.ecr.ap-northeast-1.amazonaws.com/buzzni/bpm/api",
                "essential": True,
                "name": "bpm"
            }
        ],
        "family": "bpm",
        "volumes": [],
        "placementConstraints": []
    }

    with open('task_definition.json', 'w') as outfile:
        json.dump(task_definition, outfile)


if __name__ == '__main__':
    create_task_definition(sys.argv[1])
