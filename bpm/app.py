from flask import Flask, url_for
from flask_cors import CORS
from flask_restplus import Api

from bpm.config import Config
from bpm.database import db_session
from bpm.database import init_db
from bpm.routes.v1.checks import api as check_api
from bpm.routes.v1.demo import api as demo_api
from bpm.routes.v1.logs import api as log_api
from bpm.routes.v1.rules import api as rule_api

if Config.SWAGGER_HTTPS:
    @property
    def specs_url(self):
        """Monkey patch for HTTPS"""
        return url_for(self.endpoint('specs'), _external=True, _scheme='https')


    Api.specs_url = specs_url


def create_app():
    app = Flask(__name__, template_folder='templates', static_folder='static')

    CORS(app)

    api = Api(app, version='1.0',
              title='Buzzni Process Monitoring', description="on developing")

    @api.errorhandler
    def handle_custom_exception(error):
        db_session.rollback()
        return error

    api.add_namespace(demo_api, '/api/v1/demo')
    api.add_namespace(rule_api, '/api/v1/rules')
    api.add_namespace(log_api, '/api/v1/logs')
    api.add_namespace(check_api, '/api/v1/checks')
    init_db()

    return app
