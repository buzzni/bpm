import datetime
import json

from flask import request
from flask_api import status
from flask_restplus import Resource, Namespace, fields
from sqlalchemy import select

from bpm.database import db_session
from bpm.models import Log
from bpm.schemas import log_schema
from bpm.tasks import init_task

api = Namespace('Logs', description='Logs ')

log_api_model = api.model('log', {
    'key_name': fields.String(required=True),
    'value': fields.Float,
    'host': fields.String,
    'description': fields.String,
})


@api.route('/')
class LogsListApi(Resource):
    @api.expect(log_api_model)
    def post(self):
        log_json = {} if request.json is None else request.json
        log_json.update({"ip_address": request.remote_addr})
        validations = log_schema.validate(log_json)

        if validations:
            return validations, status.HTTP_400_BAD_REQUEST
        else:
            init_task.delay(json.dumps(log_json))
        return "Accepted", status.HTTP_202_ACCEPTED

    def get(self):
        args = {key: value[0] for key, value in dict(request.args).items()}
        distinct = args.pop("distinct") if "distinct" in args else None
        if distinct:
            distinct_column = []

            item_list = db_session.query(
                getattr(Log, distinct)).distinct().all()

            for item in item_list:
                if distinct == "key_name":
                    distinct_column.append(item[0].split(".")[0])
                else:
                    distinct_column.append(item[0])
            return distinct_column, status.HTTP_200_OK
        else:
            return "", status.HTTP_400_BAD_REQUEST
