from flask import request
from flask_api import status
from flask_restplus import Resource, Namespace
from marshmallow import ValidationError

from bpm import tasks
from bpm.schemas import log_schema, rule_schema

api = Namespace('Demo', description='테스트를 위한 API')


@api.route('')
class DemoApi(Resource):
    def post(self):
        demo_json = {} if request.json is None else request.json
        demo_json["log"].update({"ip_address": request.remote_addr})
        try:
            log = log_schema.load(demo_json["log"])
        except ValidationError as validation_error:
            return validation_error.messages, status.HTTP_400_BAD_REQUEST
        try:
            rule = rule_schema.load(demo_json["rule"])
        except ValidationError as validation_error:
            return validation_error.messages, status.HTTP_400_BAD_REQUEST

        if log.key_name == rule.key_name:
            state = tasks.check_log(log, rule)
            return state, status.HTTP_200_OK

        return "key not match", status.HTTP_400_BAD_REQUEST
