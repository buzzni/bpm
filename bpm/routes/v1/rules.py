from flask import request
from flask_api import status
from flask_restplus import Resource, Namespace, fields
from marshmallow import ValidationError
from sqlalchemy.exc import StatementError, IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from bpm.models.rule import Rule
from bpm.schemas import rule_schema

api = Namespace('Rules', description='Rule의 추가/조회/삭제/변경')

condition = api.model('condition', {
    'type': fields.String,
    'boudnary1': fields.Float,
    'boundary2': fields.Float,
})
channels = api.model('channels', {
    'good': fields.String,
    'caution': fields.String,
    'emergency': fields.String,
    'timout': fields.String
})
model = api.model('Rule', {
    'name': fields.String,
    'description': fields.String,
    'owner': fields.String,
    'email': fields.String,
    'key_name': fields.String,
    'timeout': fields.Integer,
    'rule': fields.Nested(condition),
    'channels': fields.Nested(channels)
})


@api.route('/<rule_id>')
class RulesApi(Resource):
    @api.response(status.HTTP_404_NOT_FOUND, 'Not Found')
    def get(self, rule_id):
        try:
            rule = Rule.read_one(id=int(rule_id))
        except NoResultFound:
            return "not found", status.HTTP_404_NOT_FOUND
        except StatementError:
            return "argument wrong", status.HTTP_400_BAD_REQUEST
        except ValueError:
            return "value error", status.HTTP_400_BAD_REQUEST
        return rule_schema.dump(rule), status.HTTP_200_OK

    def delete(self, rule_id):
        try:
            rule = Rule.read_one(id=int(rule_id))
            rule.delete()
        except NoResultFound:
            return "notfound", status.HTTP_404_NOT_FOUND
        except ValueError:
            return "value error", status.HTTP_400_BAD_REQUEST

        return "Rule deleted " + str(rule_id), status.HTTP_204_NO_CONTENT

    def patch(self, rule_id):
        try:
            rule = Rule.read_one(id=int(rule_id))
        except NoResultFound:
            return "notfound", status.HTTP_404_NOT_FOUND
        except ValueError:
            return "value error", status.HTTP_400_BAD_REQUEST

        try:
            rule_json = rule_schema.to_model_json(request.json)
        except AttributeError:
            return "attribute error", status.HTTP_400_BAD_REQUEST

        rule.update(**rule_json)

        return rule_schema.dump(rule), status.HTTP_200_OK


@api.route('/')
class RulesListApi(Resource):
    def get(self):
        # get list
        filtering = {key: value[0] for key, value in dict(request.args).items()}

        sort = filtering.pop("sort") if "sort" in filtering else None
        page_size = filtering.pop(
            "page_size") if "page_size" in filtering else 10
        page_num = filtering.pop(
            "page_num") if "page_num" in filtering else 0

        lines = Rule.read(sort=sort, page_size=page_size, page_num=page_num,
                          **filtering)
        count = Rule.count()
        ret = rule_schema.dump(lines, many=True)

        return {"list": ret, "total": count}, status.HTTP_200_OK

    @api.expect(model)
    def post(self):
        rule_json = {} if request.json is None else request.json
        try:
            rule = rule_schema.load(rule_json)
        except ValidationError as validation_error:
            return validation_error.messages, status.HTTP_400_BAD_REQUEST
        try:
            rule.save()
            return rule_schema.dump(rule), status.HTTP_201_CREATED
        except IntegrityError as intrgrity_error:
            return intrgrity_error.statement, status.HTTP_409_CONFLICT
