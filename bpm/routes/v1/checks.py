import sqlalchemy
from flask import request
from flask_api import status
from flask_restplus import Resource, Namespace

from bpm.database import db_session
from bpm.models import Check, Log

api = Namespace('Check', description='Check된 Log를 조회하는 API')


@api.route('/')
class ChecksApi(Resource):
    def get(self, ):
        filtering = {key: value[0] for key, value in dict(request.args).items()}

        sort = filtering.pop("sort") if "sort" in filtering else None
        page_size = filtering.pop(
            "page_size") if "page_size" in filtering else 10
        page_num = filtering.pop(
            "page_num") if "page_num" in filtering else 0
        list_json = []

        query = db_session.query(Check)
        if "key_name" in filtering:
            query = query.join(Log, Check.log_id == Log.id)
        if filtering:
            for key, value in filtering.items():
                if key == "key_name":
                    query = query.filter(Log.key_name.like("%" + value + "%"))
                else:
                    query = query.filter(getattr(Check, key) == value)
        total = query.count()
        if sort is not None:
            if sort[0] == "-":
                query = query.order_by(sqlalchemy.desc(sort[1:]))
            else:
                query = query.order_by(sort)

        check_list = query.offset(int(page_size) * int(page_num)).limit(
            int(page_size))

        for item in check_list:
            list_json.append(item.to_json())

        return {"list": list_json, "total": total}, status.HTTP_200_OK
