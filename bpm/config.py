import os


class BaseConfig(object):
    MINUTE = 60
    NOTIFICATION_COOLDOWN_TIME = 10  # sec
    TIMEOUT_COOLDOWN_TIME = 1 * MINUTE


class TestConfig(BaseConfig):
    DATABASE_URL = 'sqlite:////tmp/test.db'
    REDIS_HOST = 'localhost'
    REDIS_PORT = 6379
    BROKER_REDIS_DB = 0
    CACHE_REDIS_DB = 1
    SWAGGER_HTTPS = False


class ProductionConfig(BaseConfig):
    DATABASE_URL = os.environ.get('DATABASE_URL')
    REDIS_HOST = 'session-002.stegpw.0001.apne1.cache.amazonaws.com'
    REDIS_PORT = 6379
    BROKER_REDIS_DB = 4
    CACHE_REDIS_DB = 5
    SWAGGER_HTTPS = True


if os.environ.get('BPM_SETTINGS') == 'production':
    Config = ProductionConfig

else:
    Config = TestConfig
