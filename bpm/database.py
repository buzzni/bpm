from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

from bpm.config import Config

engine = create_engine(Config.DATABASE_URL, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    # noinspection PyUnresolvedReferences
    from bpm.models import rule, log, check

    Base.metadata.create_all(bind=engine)
    print("SQLalchemy : Create Tables")
