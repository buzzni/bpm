import datetime
import json

from marshmallow import ValidationError, validates, validates_schema
from sqlalchemy import Column, String, Integer, DateTime, Float

from bpm.database import Base
from bpm.models.base import BaseSchema, CRUDMixin


class Log(Base, CRUDMixin):
    __tablename__ = "log"
    id = Column(Integer, primary_key=True, autoincrement=True)
    key_name = Column(String, nullable=False)  # Key
    value = Column(Float, nullable=False)  # Value
    host = Column(String, nullable=False)  # Agent Information
    created_at = Column(DateTime, nullable=False,
                        default=datetime.datetime.utcnow)  # Created_at
    description = Column(String)  # Description
    ip_address = Column(String, nullable=False)  # Agent's IP Address


class LogSchema(BaseSchema):
    required_field = ["key_name", "value", "host", "ip_address"]

    class Meta(BaseSchema.Meta):
        model = Log

    def load(self, *args, **kwargs):
        result = super(LogSchema, self).load(*args, **kwargs)
        if result.errors:
            raise ValidationError(str(result.errors))
        return result.data

    def dump(self, *args, **kwargs):
        result = super(LogSchema, self).dump(*args, **kwargs)
        if result.errors:
            raise ValidationError(str(result.errors))

        return result.data

    def dumps(self, *args, **kwargs):

        result = self.dump(*args, **kwargs)
        return json.dumps(result)

    @validates_schema
    def validate_required(self, data):
        for key in self.required_field:
            if key not in data.keys():
                raise ValidationError('required field is not exists')

    @validates('value')
    def validate_value(self, value):
        try:
            float(value)
        except ValueError:
            raise ValidationError('')
