import json

from marshmallow import validates, ValidationError, pre_load, \
    post_load, post_dump, validate, validates_schema
from sqlalchemy import Column, Integer, String, Float

from bpm.database import Base
from bpm.models.base import BaseSchema, CRUDMixin


class Rule(Base, CRUDMixin):
    __tablename__ = "rule"

    id = Column(Integer, autoincrement=True, unique=True, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    description = Column(String, nullable=True)

    owner = Column(String, nullable=False)
    email = Column(String, nullable=False)
    key_name = Column(String, nullable=False)
    timeout = Column(Integer, default=0)
    # rule
    type = Column(String, nullable=False)
    boundary1 = Column(Float, nullable=False)
    boundary2 = Column(Float, nullable=False)
    # channel
    channel_good = Column(String)
    channel_caution = Column(String)
    channel_emergency = Column(String)
    channel_timeout = Column(String)


class RuleSchema(BaseSchema):
    required_field = ["name", "owner", "email", "key_name", "type", "boundary1",
                      "boundary2", "channel_good", "channel_caution",
                      "channel_emergency"]

    class Meta(BaseSchema.Meta):
        model = Rule

    def load(self, *args, **kwargs):
        result = super(RuleSchema, self).load(*args, **kwargs)
        if result.errors:
            raise ValidationError(str(result.errors))
        return result.data

    def dump(self, *args, **kwargs):
        result = super(RuleSchema, self).dump(*args, **kwargs)
        if result.errors:
            raise ValidationError(str(result.errors))
        return result.data

    def dumps(self, *args, **kwargs):
        result = self.dump(*args, **kwargs)

        return json.dumps(result)

    def to_model_json(self, rule_json):
        data = {}
        if "id" in rule_json:
            raise AttributeError()
        if "name" in rule_json:
            data['name'] = rule_json['name']
        if "description" in rule_json:
            data['description'] = rule_json['description']
        if "owner" in rule_json:
            data['owner'] = rule_json['owner']
        if "email" in rule_json:
            data['email'] = rule_json['email']
        if "key_name" in rule_json:
            data['key_name'] = rule_json['key_name']
        if "timeout" in rule_json:
            data['timeout'] = rule_json['timeout']

        if "rule" in rule_json:
            if "type" in rule_json["rule"]:
                data["type"] = rule_json["rule"]["type"]
            if "boundary1" in rule_json["rule"]:
                data["boundary1"] = rule_json["rule"]["boundary1"]
            if "boundary2" in rule_json["rule"]:
                data["boundary2"] = rule_json["rule"]["boundary2"]

        if "channels" in rule_json:
            if "good" in rule_json["channels"]:
                data["channel_good"] = rule_json["channels"]["good"]
            if "caution" in rule_json["channels"]:
                data["channel_caution"] = rule_json["channels"]["caution"]
            if "emergency" in rule_json["channels"]:
                data["channel_emergency"] = rule_json["channels"]["emergency"]
            if "timeout" in rule_json["channels"]:
                data["channel_timeout"] = rule_json["channels"]["timeout"]
        return data

    @validates('email')
    def validate_email(self, value):
        validate.Email()(value)

    @validates_schema
    def validate_required(self, data):
        for key in self.required_field:
            if key not in data.keys():
                raise ValidationError('required field is not exists')

    @post_load
    def validate(self, data):
        if data.boundary1 > data.boundary2:
            raise ValidationError('boundary2 must be greater or equal to '
                                  'boundary1 in Type A')
        return data

    @pre_load
    def pre_load(self, data):
        try:
            data['type'] = data['rule']['type']
            data['boundary1'] = data['rule']['boundary1']
            data['boundary2'] = data['rule']['boundary2']
            data['channel_good'] = json.dumps(data['channels']['good'])
            data['channel_caution'] = json.dumps(data['channels']['caution'])
            data['channel_emergency'] = json.dumps(data['channels']['emergency'])
            if "timeout" in data:
                if "timeout" not in data['channels']:
                    raise ValidationError("channel_timeout is not exists")
                data['channel_timeout'] = json.dumps(data['channels']['timeout'])
            else:
                if "timeout" in data['channels']:
                    raise ValidationError("timeout is not exists")
                data["timeout"] = 0
                data['channel_timeout'] = "[]"
        except Exception as e:
            raise ValidationError(e)

        return data

    @post_dump
    def post_dump(self, data):

        data['rule'] = {
            'type': data.pop('type'),
            'boundary1': data.pop('boundary1'),
            'boundary2': data.pop('boundary2')
        }
        good = json.loads(data.pop('channel_good'))
        caution = json.loads(data.pop('channel_caution'))
        emergency = json.loads(data.pop('channel_emergency'))
        timeout = json.loads(data.pop('channel_timeout'))
        data['channels'] = {
            'good': good,
            'caution': caution,
            'emergency': emergency,
            'timeout': timeout
        }
        return data
