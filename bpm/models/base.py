import json

import sqlalchemy
from flask import json
from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy.exc import IntegrityError

from bpm.database import db_session


class CRUDMixin(object):
    def fill(self, **kwargs):
        for name in kwargs.keys():
            if name == "channel_good" or \
                    name == "channel_caution" or \
                    name == "channel_emergency":
                setattr(self, name, json.dumps(kwargs[name]))
            else:
                setattr(self, name, kwargs[name])
        return self

    @classmethod
    def create(cls, only_flush=False, **kwargs):
        return cls().fill(**kwargs).save(only_flush=only_flush)

    def save(self, only_flush=False):
        try:
            db_session.add(self)
            if only_flush:
                db_session.flush()

            else:
                db_session.commit()

        except IntegrityError as integrity_error:
            db_session.rollback()
            raise integrity_error

    @classmethod
    def read(cls, sort=None, page_size=10, page_num=0, **kwargs):
        query = db_session.query(cls)

        if kwargs:
            for key, value in kwargs.items():
                query = query.filter(getattr(cls, key) == value)
        # sorting 일단 한개만
        if sort is not None:
            if sort[0] == "-":
                query = query.order_by(sqlalchemy.desc(sort[1:]))
            else:
                query = query.order_by(sort)

        return query.offset(int(page_size) * int(page_num)).limit(
            int(page_size))

    @classmethod
    def count(cls, **kwargs):
        query = db_session.query(cls)
        if kwargs:
            for key, value in kwargs.items():
                query = query.filter(getattr(cls, key) == value)
        count = query.count()

        return count

    @classmethod
    def read_one(cls, **kwargs):
        query = db_session.query(cls)
        if kwargs:
            for key, value in kwargs.items():
                query = query.filter(getattr(cls, key) == value)
        return query.one()

    def update(self, **kwargs):
        member = [attr for attr in dir(self) if
                  not callable(getattr(self, attr)) and not attr.startswith(
                      "__") and not attr.startswith("_")]

        for key, value in kwargs.items():
            if key not in member:
                raise AttributeError

        self.fill(**kwargs)
        db_session.commit()

    def delete(self, only_flush=False):
        try:
            db_session.delete(self)
            if only_flush:
                db_session.flush()
            else:
                db_session.commit()

        except IntegrityError as integrity_error:
            db_session.rollback()
            raise integrity_error


class BaseSchema(ModelSchema):
    class Meta:
        sqla_session = db_session
