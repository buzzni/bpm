import datetime
import time

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship, backref

from bpm.database import Base
from bpm.models.base import CRUDMixin


class Check(Base, CRUDMixin):
    __tablename__ = 'check'

    id = Column(Integer, autoincrement=True, unique=True, primary_key=True)
    rule_id = Column(Integer, ForeignKey('rule.id'))
    log_id = Column(Integer, ForeignKey('log.id'))
    state = Column(String)

    rule = relationship("Rule", backref=backref('check'), order_by=id)
    log = relationship("Log", backref=backref('check', order_by=id))

    def to_json(self):
        return {
            "log_id": self.log_id,
            "rule_id": self.rule_id,
            "log_key_name": self.log.key_name,
            "log_value": self.log.value,
            "created_at": datetime.datetime.strftime(
                utc2local(self.log.created_at), '%Y-%m-%d %H:%M:%S'),
            "log_host": self.log.host,
            "log_ip_address": self.log.ip_address,
            "state": self.state
        }


def utc2local(utc):
    epoch = time.mktime(utc.timetuple())
    offset = datetime.datetime.fromtimestamp(
        epoch) - datetime.datetime.utcfromtimestamp(epoch)
    return utc + offset
