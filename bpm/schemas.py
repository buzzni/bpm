from bpm.models.log import LogSchema
from bpm.models.rule import RuleSchema

rule_schema = RuleSchema()
log_schema = LogSchema()
