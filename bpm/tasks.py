# Celery Tasks
import json
import logging
from datetime import datetime

import redis
import requests
from celery import Celery
from celery.signals import worker_process_init

from bpm.config import Config
from bpm.database import db_session, init_db
from bpm.models import Check, Rule, Log
from bpm.schemas import log_schema

accept_content = ['json']
# This code must be loacted Logic Server
app = Celery('tasks', broker='redis://{}:{}/{}'.format(Config.REDIS_HOST,
                                                       Config.REDIS_PORT,
                                                       Config.BROKER_REDIS_DB))
# Redis DB(1) for cool_time
cool_redis = redis.StrictRedis(host=Config.REDIS_HOST,
                               port=Config.REDIS_PORT,
                               db=Config.CACHE_REDIS_DB)


@app.task(bind=True)
def init_task(self, log_json):
    # input = String(validated_log)
    # output = check_log(log_model, rule_model)
    log_model = log_schema.load(json.loads(log_json))
    matching_rules = db_session.query(Rule).filter(
        Rule.key_name == log_model.key_name).all()
    if matching_rules:
        for rule_model in matching_rules:
            state = check_log(log_model=log_model, rule_model=rule_model)
            if state:
                write_to_database(log_model=log_model,
                                  rule_model=rule_model, state=state)
                noti_handler(log_model=log_model,
                             rule_model=rule_model, state=state)
            else:
                pass
    else:
        pass


def check_log(log_model, rule_model):
    # input = 1 log_model(dict), 1 rule(Model)
    lval = float(log_model.value)
    boundary1 = rule_model.boundary1
    boundary2 = rule_model.boundary2
    rule_type = rule_model.type
    state = ''

    # If type == 'A'
    # Good < Boundary1 <= Caution < Boundary2 <= Error
    if rule_type == 'A':
        if lval < boundary1:
            state = 'Good'
        elif boundary1 <= lval < boundary2:
            state = 'Caution'
        elif boundary2 <= lval:
            state = 'Emergency'
        else:
            raise RuntimeError
    # If type == 'B'
    # Error < Boundary1 <= Caution < boundary2 <= Good
    elif rule_type == 'B':
        if lval < boundary1:
            state = 'Emergency'
        elif boundary1 <= lval < boundary2:
            state = 'Caution'
        elif boundary2 <= lval:
            state = 'Good'
        else:
            raise RuntimeError
    else:
        raise NotImplementedError

    return state


def write_to_database(log_model, rule_model, state):
    log_model.save(only_flush=True)
    Check.create(log_id=log_model.id, rule_id=rule_model.id, state=state,
                 only_flush=True)

    db_session.commit()


def noti_handler(log_model, rule_model, state):
    expired = cool_redis.ttl(rule_model.name)
    # if rule_model.name in the redis_cache, then skip all jobs
    if expired > 0:
        pass
    else:
        cool_redis.setex(
            rule_model.name, Config.NOTIFICATION_COOLDOWN_TIME, "")

        if state == 'Good':
            channels = json.loads(rule_model.channel_good)
        elif state == 'Caution':
            channels = json.loads(rule_model.channel_caution)
        elif state == 'Emergency':
            channels = json.loads(rule_model.channel_emergency)
            channels.append("default")
        else:
            channels = []

        for channel in channels:
            message = get_message(log_model, rule_model, state, channel)
            body = {"message": message}
            request_to_watup(channel, body)


def request_to_watup(channel, body):
    watup_url = "https://watup.buzzni.net/v1/push/channels/"
    headers = {'Content-Type': 'application/json',
               'Authorization': 'Basic YnV6em5pOjY2ODg='}
    complete_url = watup_url + channel + '/'

    return requests.post(complete_url, headers=headers, json=body)


def get_rule_message(rule_model):
    return {
        "name": rule_model.name,
        "description": rule_model.description,
        "owner": rule_model.owner,
        "email": rule_model.email
    }


def get_message(log_model, rule_model, state, channel, detail=True):
    if detail:
        message = '[{0}] {1}: {2}, [{3}] [{4}]'. \
            format(state,
                   log_model.key_name,
                   str(log_model.value),
                   rule_model.description or "",
                   log_model.description or "")

    else:
        json_format = {
            "level": state,
            "channel": channel,
            "agent": {
                "host": log_model.host,
                "ip_address": log_model.ip_address
            },
            "log": {
                "key_name": log_model.key_name,
                "value": log_model.value,
                "created_at": log_model.created_at.strftime(
                    "%Y-%m-%d %H:%M:%S"),
                "description": log_model.description
            },
            "rule": get_rule_message(rule_model)
        }
        message = json.dumps(json_format, ensure_ascii=False)

    return message


def check_timeout(rule):
    query = db_session.query(Check, Log)
    query = query.filter(Check.rule_id == rule.id)
    query = query.order_by(Log.created_at.desc())

    last_log = query.first()

    if not last_log:
        return
    logging.warning(
        datetime.utcnow().strftime("%Y%m%d %H:%M:%S") + "," + last_log[
            1].created_at.strftime("%Y%m%d %H:%M:%S") + "," + str(rule.timeout))

    if datetime.utcnow() > last_log[1].created_at \
            and (
            datetime.utcnow() - last_log[1].created_at).seconds >= rule.timeout:
        # Set timeout cooltime
        cool_redis.setex(rule.name + "timeout",
                         Config.TIMEOUT_COOLDOWN_TIME, "")
        for channel in json.loads(rule.channel_timeout):
            request_to_watup(
                channel, {'message': "[Timeout] {}의 {}".format(rule.name,
                                                               rule.key_name)})


@app.task
def periodic_timeout_checker():
    rules = Rule.read()
    for rule in rules:
        if rule.timeout:
            # Check timeout cooltime
            if cool_redis.ttl(rule.name + "timeout") < 0:
                check_timeout(rule)


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    sender.add_periodic_task(10, periodic_timeout_checker.s(),
                             name='check every 10s',
                             expires=10)


@worker_process_init.connect
def celery_worker_init_db(**_):
    init_db()
