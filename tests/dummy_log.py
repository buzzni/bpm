# Frome Agent
# valid
valid_log_from_agent = {
    "key_name": "asdf",
    "value": 50,
    "host": "zxcv",
    "description": "this is mock valid log",
}
valid_log_from_agent_without_description = {
    "key_name": "asdf",
    "value": 50,
    "host": "zxcv",
}
# invalid
string_value_log_from_agent = {
    "key_name": "asdf",
    "value": "qwer",
    "host": "zxcv",
    "description": "this is mock invalid log",
}

# invalid
missing_field_log_from_agent = {
    "key_name": "asdf",
    "value": 50,
    "description": "this is mock invalid log",
}
# From Api


valid_log_from_api = {
    "key_name": "asdf",
    "value": 50,
    "host": "zxcv",
    "ip_address": "127.0.0.1",
    "description": "this is mock valid log",
}

valid_float_value_log_from_api_with = {
    "key_name": "asdf",
    "value": 50.5,
    "host": "zxcv",
    "ip_address": "127.0.0.1",
    "description": "this is mock valid log",
}

valid_log_from_api_without_description = {
    "key_name": "asdf",
    "value": 50,
    "host": "zxcv",
    "ip_address": "127.0.0.1",
}
# invalid
string_value_log_from_api = {
    "key_name": "asdf",
    "value": "qwer",
    "host": "zxcv",
    "ip_address": "127.0.0.1",
    "description": "this is mock invalid log",
}

# invalid
missing_field_log_from_api = {
    "key_name": "asdf",
    "value": 50,
    "description": "this is mock invalid log",
}
no_channel_log = {
    "key_name": "stress",
    "value": 50,
    "host": "Marco",
    "ip_address": "127.0.0.1",
    "description": "no channel log",
}

one_channel_log = {
    "key_name": "stress",
    "value": 93,
    "host": "Marco",
    "ip_address": "127.0.0.1",
    "description": "no channel log",
}

multi_channel_log = {
    "key_name": "stress",
    "value": 98,
    "host": "Marco",
    "ip_address": "127.0.0.1",
    "description": "no channel log",
}
