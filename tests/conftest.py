import pytest

from bpm.app import create_app
from bpm.database import db_session, init_db, engine, Base
from bpm.models import Check
from bpm.models.log import LogSchema
from bpm.models.rule import RuleSchema, Rule
from bpm.models.log import Log
import datetime
import json


@pytest.fixture
def rule_schema():
    return RuleSchema()


@pytest.fixture
def log_schema():
    return LogSchema()


@pytest.fixture(scope='session')
def flask_app():
    app = create_app()

    app_context = app.app_context()
    app_context.push()
    # app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
    yield app
    app_context.pop()


@pytest.fixture(scope='session')
def db():
    Base.metadata.drop_all(bind=engine)
    init_db()
    yield db_session

    # Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope='session')
def flask_client(flask_app):
    return flask_app.test_client()


@pytest.fixture(scope='function')
def session(db):
    connection = engine.connect()
    trans = connection.begin()
    print(db_session)
    yield db_session

    db_session.rollback()
    trans.rollback()
    db_session.close()


@pytest.fixture(scope='function')
def rule(session):
    rule = Rule(name='testrule',
                description='rule for testing',
                owner='Marco',
                email='marco@buzzni.com',
                key_name='stress',
                type='A',
                boundary1=30,
                boundary2=60,
                timeout=10,
                channel_good='["1", "2"]',
                channel_caution='["1", "2"]',
                channel_emergency='["1", "2"]',
                channel_timeout='["1", "2"]')
    session.add(rule)
    session.flush()
    return rule


@pytest.fixture(scope='function')
def log(session):
    log = Log(
        key_name='stress',
        value=50,
        host='From Marco',
        ip_address='123.132.213')
    session.add(log)
    session.flush()
    return log


@pytest.fixture(scope='function')
def check(session, rule, log):
    check = Check(
        rule_id=rule.id,
        log_id=log.id,
        state="caution"
    )
    session.add(check)
    session.flush()
    return check


@pytest.fixture(scope='function')
def rule1(session):
    rule = Rule(name='test_rule1',
                description='Test Rule 1',
                owner='Dean',
                email='Dean@buzzni.com',
                key_name='stress',
                type='A',
                boundary1=30,
                boundary2=60,
                timeout=10,
                channel_good='["1", "2"]',
                channel_caution='["1", "2"]',
                channel_emergency='["1", "2"]',
                channel_timeout='["1", "2"]')
    session.add(rule)
    session.flush()
    return rule


@pytest.fixture(scope='function')
def rule2(session):
    rule = Rule(name='test_rule2',
                description='Rule 2 Test',
                owner='Dean',
                email='Dean@buzzni.com',
                key_name='stress',
                type='A',
                boundary1=90,
                boundary2=95,
                channel_good='[]',
                channel_caution='["channel_1"]',
                channel_emergency='["channel_1", "channel_2"]')
    session.add(rule)
    session.flush()
    return rule


@pytest.fixture(scope='function')
def empty_channel_rule(session):
    rule = Rule(name='no_emergency',
                description='This rule doesn\'t have emergency channel',
                owner='Marco',
                email='Marco@buzzni.com',
                key_name='stress',
                type='A',
                boundary1=50,
                boundary2=80,
                channel_good='[]',
                channel_caution='[]',
                channel_emergency='[]')
    session.add(rule)
    session.flush()
    return rule


@pytest.fixture(scope='function')
def empty_timeout_rule(session):
    rule = Rule(name='empty_timeout_rule',
                description='empty_timeout_rule Test',
                owner='Marco',
                email='Marco@buzzni.com',
                key_name='stress',
                type='A',
                boundary1=90,
                boundary2=95,
                channel_good='[]',
                channel_caution='["channel_1"]',
                channel_emergency='["channel_1", "channel_2"]')
    session.add(rule)
    session.flush()
    return rule


@pytest.fixture(scope='function')
def valid_timeout_rule(session):
    rule = Rule(name='valid_timeout_rule',
                description='valid_timeout_rule Test',
                owner='Marco',
                email='Marco@buzzni.com',
                key_name='stress',
                timeout=60,
                type='A',
                boundary1=90,
                boundary2=95,
                channel_good='[]',
                channel_caution='["channel_1"]',
                channel_emergency='["channel_1", "channel_2"]')
    session.add(rule)
    session.flush()
    return rule


@pytest.fixture(scope='function')
def log1():
    log = {
        "key_name": "stress",
        "value": 50,
        "host": "From Marco",
        "ip_address": "123.132.213"
    }
    # json to string
    return json.dumps(log)


@pytest.fixture(scope='function')
def log2():
    log = {
        "key_name": "nothing",
        "value": 155,
        "host": "malignant",
        "ip_address": "123.132.213"
    }
    # json to string
    return json.dumps(log)


@pytest.fixture(scope='function')
def log1_model():
    log_model = Log(
        key_name='stress',
        value=50,
        host='From Marco',
        ip_address='123.132.213')

    return log_model


@pytest.fixture(scope='function')
def log2_model():
    log_model = Log(
        key_name='stress',
        value=155,
        host='malignant',
        ip_address='123.132.213')
    return log_model


@pytest.fixture(scope='function')
def no_channel_log_model():
    log_model = Log(
        key_name="stress",
        value=50,
        host="Marco",
        ip_address="127.0.0.1",
        created_at=datetime.datetime.now(),
        description="no channel log")
    return log_model


@pytest.fixture(scope='function')
def one_channel_log_model():
    log_model = Log(
        key_name="stress",
        value=93,
        host="Marco",
        ip_address="127.0.0.1",
        created_at=datetime.datetime.now(),
        description="no channel log")
    return log_model


@pytest.fixture(scope='function')
def multi_channel_log_model():
    log_model = Log(
        key_name="stress",
        value=98,
        host="Marco",
        ip_address="127.0.0.1",
        created_at=datetime.datetime.now(),
        description="no channel log")
    return log_model
