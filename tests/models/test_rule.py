import json

import pytest
from marshmallow import ValidationError

from bpm.models.rule import Rule
from tests.dummy_rule import valid_rule, reverse_boundary_rule, \
    missing_key_name_field_rule, missing_owner_field_rule, \
    missing_rule_field_rule, missing_channel_field_rule

mock_rule = Rule(name='testrule',
                 description='rule for testing',
                 owner='Marco',
                 email='marco@buzzni.com',
                 key_name='stress',
                 type='A',
                 boundary1=30,
                 boundary2=60,
                 timeout=100,
                 channel_good='["1", "2"]',
                 channel_caution='["1", "2"]',
                 channel_emergency='["1", "2"]',
                 channel_timeout='["1", "2"]')

reverse_boundary_mock_rule = Rule(name='testrule',
                                  description='rule for testing',
                                  owner='Marco',
                                  email='marco@buzzni.com',
                                  key_name='stress',
                                  type='A',
                                  boundary1=60,
                                  boundary2=30,
                                  timeout=100,
                                  channel_good='["1", "2"]',
                                  channel_caution='["1", "2"]',
                                  channel_emergency='["1", "2"]',
                                  channel_timeout='["1", "2"]')


class TestRuleSchema(object):
    def test_load(self, rule_schema):
        rule = rule_schema.load(valid_rule)

        assert isinstance(rule, Rule)
        assert rule.name == valid_rule['name']
        assert rule.description == valid_rule['description']
        assert rule.type == valid_rule['rule']['type']
        assert rule.channel_good == json.dumps(valid_rule['channels']['good'])
        assert rule.timeout == valid_rule['timeout']
        assert rule.channel_timeout == \
               json.dumps(valid_rule['channels']['timeout'])

    def test_load_without_timeout(self, rule_schema):
        valid_rule_without_timeout = {
            "name": "test_rule",
            "description": "rule for testing",
            "owner": "Marco",
            "email": "marco@buzzni.com",
            "key_name": "stress",
            "rule": {
                "type": "A",
                "boundary1": 30,
                "boundary2": 50
            },
            "channels": {
                "good": ["1", "2"],
                "caution": ["1", "2"],
                "emergency": ["1", "2"],
            }
        }

        rule = rule_schema.load(valid_rule_without_timeout)
        assert isinstance(rule, Rule)
        assert rule.timeout == 0
        assert rule.channel_timeout == '[]'

    def test_load_only_timeout(self, rule_schema):
        valid_rule_without_timeout = {
            "name": "test_rule",
            "description": "rule for testing",
            "owner": "Marco",
            "email": "marco@buzzni.com",
            "key_name": "stress",
            "timeout": 100,
            "rule": {
                "type": "A",
                "boundary1": 30,
                "boundary2": 50
            },
            "channels": {
                "good": ["1", "2"],
                "caution": ["1", "2"],
                "emergency": ["1", "2"],
            }
        }
        with pytest.raises(ValidationError):
            rule_schema.load(valid_rule_without_timeout)

    def test_load_only_channel_timeout(self, rule_schema):
        valid_rule_without_timeout = {
            "name": "test_rule",
            "description": "rule for testing",
            "owner": "Marco",
            "email": "marco@buzzni.com",
            "key_name": "stress",
            "rule": {
                "type": "A",
                "boundary1": 30,
                "boundary2": 50
            },
            "channels": {
                "good": ["1", "2"],
                "caution": ["1", "2"],
                "emergency": ["1", "2"],
                "timeout": ["1", "2"]
            }
        }
        with pytest.raises(ValidationError):
            rule_schema.load(valid_rule_without_timeout)

    def test_load_invalid(self, rule_schema):
        with pytest.raises(ValidationError):
            rule_schema.load(reverse_boundary_rule)
        with pytest.raises(ValidationError):
            rule_schema.load(missing_key_name_field_rule)
        with pytest.raises(ValidationError):
            rule_schema.load(missing_owner_field_rule)
        with pytest.raises(ValidationError):
            rule_schema.load(missing_rule_field_rule)
        with pytest.raises(ValidationError):
            rule_schema.load(missing_channel_field_rule)

    def test_validate(self, rule_schema):
        assert rule_schema.validate(mock_rule) == mock_rule

    def test_validate_fail(self, rule_schema):
        with pytest.raises(ValidationError):
            rule_schema.validate(reverse_boundary_mock_rule)

    def test_dumps(self, rule_schema):
        string_json = rule_schema.dumps(mock_rule)
        assert isinstance(rule_schema.dumps(mock_rule), str)
        json_object = json.loads(string_json)
        assert isinstance(json_object, dict)
        print(json_object)
        assert json_object['channels']['good'] == ['1', '2']

    def test_dump(self, rule_schema):
        json_object = rule_schema.dump(mock_rule)
        assert isinstance(json_object, dict)
        assert json_object['channels']['good'] == ['1', '2']

    def test_to_model_json(self, rule_schema):
        rule_json = {
            "owner": "patch",
            "key_name": "patchtest",
            "rule": {
                "type": "A",
                "boundary1": 20,
                "boundary2": 50,
            },
            "channels": {
                "good": '["1", "2"]',
                "caution": '["1", "2"]',
                "emergency": '["1", "2"]'}
        }
        patched_rule = rule_schema.to_model_json(rule_json)

        assert patched_rule["key_name"] == rule_json["key_name"]
        assert patched_rule["type"] == rule_json["rule"]["type"]
        assert patched_rule["boundary1"] == rule_json["rule"]["boundary1"]
        assert patched_rule["boundary2"] == rule_json["rule"]["boundary2"]
        assert patched_rule["channel_good"] == rule_json["channels"]["good"]
        assert patched_rule["channel_caution"] == rule_json["channels"][
            "caution"]
        assert patched_rule["channel_emergency"] == rule_json["channels"][
            "emergency"]

    def test_to_model_json_has_id(self, rule_schema):
        rule_json = {
            "id": "123"
        }
        with pytest.raises(AttributeError):
            rule_schema.to_model_json(rule_json)
