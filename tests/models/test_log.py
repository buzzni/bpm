import datetime

import pytest
from marshmallow import ValidationError, utils

from bpm.models.log import Log
from tests.dummy_log import valid_log_from_api, \
    valid_log_from_api_without_description, \
    string_value_log_from_api, missing_field_log_from_api,\
    valid_float_value_log_from_api_with

mock_log = Log(key_name="asdf",
               value=50.5,
               host="zxcv",
               created_at=datetime.datetime.now(),
               description="this is mock log",
               ip_address="127.0.0.1")

mock_log_without_description = Log(key_name="asdf",
                                   value=50.5,
                                   host="zxcv",
                                   created_at=datetime.datetime.now(),
                                   ip_address="127.0.0.1")


class TestLogSchema(object):
    @pytest.mark.parametrize("valid_log", [
        valid_log_from_api,
        valid_float_value_log_from_api_with])
    def test_load(self, log_schema, valid_log):
        # request.remote_addr = "127.0.0.1"

        log = log_schema.load(valid_log)

        assert isinstance(log, Log)
        assert valid_log["key_name"] == log.key_name
        assert valid_log["value"] == log.value
        assert valid_log["host"] == log.host
        assert log.created_at is None

        assert valid_log["description"] == log.description
        assert valid_log["ip_address"] == log.ip_address

    def test_load_without_description(self, log_schema):
        # request.remote_addr = "127.0.0.1"

        log = log_schema.load(valid_log_from_api_without_description)
        assert isinstance(log, Log)
        assert valid_log_from_api_without_description[
                   "key_name"] == log.key_name
        assert valid_log_from_api_without_description["value"] == log.value
        assert valid_log_from_api_without_description["host"] == log.host
        assert log.created_at is None

        assert not log.description
        assert valid_log_from_api_without_description[
                   "ip_address"] == log.ip_address

    def test_dump(self, log_schema):
        json_object = log_schema.dump(mock_log)
        assert isinstance(json_object, dict)
        assert json_object["key_name"] == mock_log.key_name
        assert json_object["value"] == mock_log.value
        assert json_object["host"] == mock_log.host

        assert json_object["ip_address"] == mock_log.ip_address
        assert json_object["description"] == mock_log.description
        assert utils.from_iso(json_object["created_at"]).strftime(
            '%Y-%m-%d %H:%M:%S') == mock_log.created_at.strftime(
            '%Y-%m-%d %H:%M:%S')

    def test_dump_without_description(self, log_schema):
        json_object = log_schema.dump(mock_log_without_description)
        assert isinstance(json_object, dict)
        assert json_object["key_name"] == mock_log_without_description.key_name
        assert json_object["value"] == mock_log_without_description.value
        assert json_object["host"] == mock_log_without_description.host

        assert json_object["ip_address"] == \
               mock_log_without_description.ip_address
        assert json_object["description"] == \
               mock_log_without_description.description
        assert utils.from_iso(json_object["created_at"]).strftime(
            '%Y-%m-%d %H:%M:%S') == mock_log.created_at.strftime(
            '%Y-%m-%d %H:%M:%S')

    def test_load_invalid(self, log_schema):
        with pytest.raises(ValidationError):
            log_schema.load(string_value_log_from_api)

        with pytest.raises(ValidationError):
            log_schema.load(missing_field_log_from_api)
