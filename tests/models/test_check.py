import datetime

from bpm.models.check import utc2local


class TestCheckModel(object):
    def test_check_to_json(self, check):
        check_json = check.to_json()

        isinstance(check_json, dict)
        assert check_json["log_id"] == check.log_id
        assert check_json["rule_id"] == check.rule_id
        assert check_json["created_at"] == datetime.datetime.strftime(
            utc2local(check.log.created_at), '%Y-%m-%d %H:%M:%S')
        assert check_json["log_key_name"] == check.log.key_name
        assert check_json["log_value"] == check.log.value
        assert check_json["log_host"] == check.log.host
        assert check_json["log_ip_address"] == check.log.ip_address
        assert check_json["state"] == check.state
