from unittest.mock import patch

import pytest
from sqlalchemy import Column, String, Integer, desc

from bpm.database import Base
from bpm.database import db_session
from bpm.models.base import CRUDMixin


class FakeModel(Base, CRUDMixin):
    __tablename__ = "test"
    id = Column(Integer, primary_key=True, autoincrement=True)
    key_name = Column(String)
    value = Column(Integer)


class TestCRUDMixin(object):

    def test_fill(self):
        crud_mixin = FakeModel()
        crud_mixin.fill(key_name='name', value=233)

        assert crud_mixin.key_name == 'name'
        assert crud_mixin.value == 233

    @patch.object(CRUDMixin, 'fill')
    def test_create(self, mock_fill):
        FakeModel.create(key_name='name', value=233)

        mock_fill.assert_called_with(key_name='name', value=233)
        mock_fill().save.assert_called_with(only_flush=False)

    @patch.object(CRUDMixin, 'fill')
    def test_create_only_flush(self, mock_fill):
        FakeModel.create(key_name='name', value=233, only_flush=True)

        mock_fill.assert_called_with(key_name='name', value=233)
        mock_fill().save.assert_called_with(only_flush=True)

    @patch.object(db_session, 'add')
    @patch.object(db_session, 'commit')
    def test_save(self, mock_commit, mock_add):
        fake_model = FakeModel()
        fake_model.save()

        mock_add.assert_called_once_with(fake_model)
        mock_commit.assert_called_once()

    @patch.object(db_session, 'add')
    @patch.object(db_session, 'flush')
    def test_save_only_flush(self, mock_flush, mock_add):
        fake_model = FakeModel()
        fake_model.save(only_flush=True)

        mock_add.assert_called_once_with(fake_model)
        mock_flush.assert_called_once()

    @patch.object(db_session, 'query')
    def test_read(self, mock_query):
        FakeModel.read()
        mock_query.assert_called_once_with(FakeModel)
        mock_query().offset.assert_called_once_with(0)
        mock_query().offset().limit.assert_called_once_with(10)

    @patch.object(db_session, 'query')
    def test_read_with_page(self, mock_query):
        FakeModel.read(page_size="10", page_num="5")
        mock_query.assert_called_with(FakeModel)

        mock_query().offset.assert_called_once_with(10 * 5)
        mock_query().offset().limit.assert_called_once_with(10)

    @patch('sqlalchemy.desc')
    @patch.object(db_session, 'query')
    def test_read_with_sorting_desc(self, mock_query, desc1):
        FakeModel.read(sort="-log_id")
        mock_query.assert_called_with(FakeModel)

        mock_query().order_by.assert_called_once_with(desc1("log_id"))
        mock_query().order_by().offset.assert_called_once_with(0)
        mock_query().order_by().offset().limit.assert_called_once_with(10)

    @patch.object(db_session, 'query')
    def test_read_with_sorting(self, mock_query):
        FakeModel.read(sort="log_id")
        mock_query.assert_called_with(FakeModel)
        mock_query().order_by.assert_called_once_with("log_id")
        mock_query().order_by().all.assert_called_once()

    @patch.object(db_session, 'query')
    def test_read_with_kwargs(self, mock_query):
        FakeModel.read(page_num=0, page_size=10, sort=None, key_name='name',
                       value=233)
        mock_query.assert_called_with(FakeModel)
        mock_query().filter.assert_called_once()
        mock_query().filter().filter.assert_called_once()
        mock_query().filter().filter().offset.assert_called_once()
        mock_query().filter().filter().offset().limit.assert_called_once()

    @patch.object(db_session, 'commit')
    @patch.object(CRUDMixin, 'fill')
    def test_update(self, mock_fill, mock_commit):
        # args 가 없을 때??
        # fake_model = FakeModel()
        # fake_model.update(a=1, b=2)
        # mock_fill.assert_called_with(a=1, b=2)
        # mock_fill().save.assert_called_with()
        fake_model = FakeModel()
        fake_model.update(key_name=1, value=2)
        mock_fill.assert_called_with(key_name=1, value=2)

        mock_commit.assert_called_once()

    @patch.object(CRUDMixin, 'fill')
    def test_update_not_exist_key(self, mock_fill):
        # When arg's key not exist
        # fake_model = FakeModel()
        # fake_model.update(a=1, b=2)
        # mock_fill.assert_called_with(a=1, b=2)
        # mock_fill().save.assert_called_with()
        fake_model = FakeModel()
        with pytest.raises(AttributeError):
            fake_model.update(a=1, b=2)
        mock_fill.assert_not_called()

        mock_fill().save.assert_not_called()

    @patch.object(db_session, 'delete')
    @patch.object(db_session, 'flush')
    def test_delete_flush(self, mock_flush, mock_delete):
        fake_model = FakeModel()
        fake_model.delete(only_flush=True)

        mock_delete.assert_called_with(fake_model)
        mock_flush.assert_called_with()

    @patch.object(db_session, 'delete')
    @patch.object(db_session, 'commit')
    def test_delete_noflush(self, mock_commit, mock_delete):
        fake_model = FakeModel()
        fake_model.delete()

        mock_delete.assert_called_with(fake_model)
        mock_commit.assert_called_with()

    @patch.object(db_session, 'query')
    def test_read_with_sorting(self, mock_query):
        FakeModel.count(key_name='name')
        mock_query.assert_called_with(FakeModel)
        mock_query().filter.assert_called_once()
        mock_query().filter().count.assert_called_once()
