valid_rule = {
    "name": "test_rule",
    "description": "rule for testing",
    "owner": "Marco",
    "email": "marco@buzzni.com",
    "key_name": "stress",
    "timeout": 10,
    "rule": {
        "type": "A",
        "boundary1": 30.2,
        "boundary2": 50.4
    },
    "channels": {
        "good": ["1", "2"],
        "caution": ["1", "2"],
        "emergency": ["1", "2"],
        "timeout": ["1"]
    }
}
valid_rule2 = {
    "name": "test_rule",
    "description": "rule for testing",
    "owner": "Dean",
    "email": "dean@buzzni.com",
    "key_name": "happy",
    "rule": {
        "type": "A",
        "boundary1": 30.3,
        "boundary2": 50.6
    },
    "channels": {
        "good": ["1", "2"],
        "caution": ["1", "2"],
        "emergency": ["1", "2"],
        "timeout": ["1"]
    }
}
# invalid_rule : boundary reverse
reverse_boundary_rule = {
    "name": "test_invalid_rule1",
    "description": "rule for testing",
    "owner": "Dean",
    "email": "marco@buzzni.com",
    "key_name": "stress",
    "timeout": 10,
    "rule": {
        "type": "A",
        "boundary1": 60,
        "boundary2": 20
    },
    "channels": {
        "good": ["1", "2"],
        "caution": ["1", "2"],
        "emergency": ["1", "2"],
        "timeout": ["1"]
    }
}
# invalid_rule : missing field
missing_key_name_field_rule = {
    "name": "test_invalid_rule2",
    "description": "rule for testing",
    "owner": "Jason",
    "email": "json@buzzni.com",

    "rule": {
        "type": "A",
        "boundary1": 20,
        "boundary2": 60
    },
    "channels": {
        "good": ["1", "2"],
        "caution": ["1", "2"],
        "emergency": ["1", "2"],
        "timeout": ["1"]
    }
}
missing_owner_field_rule = {
    "name": "test_invalid_rule2",
    "description": "rule for testing",
    "email": "json@buzzni.com",
    "key_name": "stress",
    "timeout": 10,
    "rule": {
        "type": "A",
        "boundary1": 20,
        "boundary2": 60
    },
    "channels": {
        "good": ["1", "2"],
        "caution": ["1", "2"],
        "emergency": ["1", "2"],
        "timeout": ["1"]
    }
}

missing_rule_field_rule = {
    "name": "test_rule",
    "description": "rule for testing",
    "owner": "Marco",
    "email": "marco@buzzni.com",
    "key_name": "stress",
    "channels": {
        "good": ["1", "2"],
        "caution": ["1", "2"],
        "emergency": ["1", "2"]
    }
}
missing_channel_field_rule = {
    "name": "test_rule",
    "description": "rule for testing",
    "owner": "Marco",
    "email": "marco@buzzni.com",
    "key_name": "stress",
    "rule": {
        "type": "A",
        "boundary1": 20,
        "boundary2": 60
    }
}
