import json
from datetime import datetime
from unittest.mock import patch, call

import pytest
import requests
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.query import Query

from bpm import tasks
from bpm.database import db_session
from bpm.models import Rule, Log, Check
from bpm.schemas import log_schema
from bpm.tasks import get_message, write_to_database
from tests.dummy_log import no_channel_log, one_channel_log, multi_channel_log


@patch.object(tasks, 'write_to_database')
@patch.object(tasks, 'noti_handler')
@patch.object(log_schema, 'load')
@patch.object(tasks, 'check_log')
def test_init_task_macthing_one(check_log, load, noti_handler,
                                write_to_database, log1, rule1):
    # input = request.json(log)
    # output = Calling check_log()
    load.return_value = Log(
        key_name='stress',
        value=50.5,
        host='From Marco',
        ip_address='123.132.213')
    tasks.init_task(log1)
    check_log.assert_called_once()
    noti_handler.assert_called_once()
    write_to_database.assert_called_once()


@patch.object(tasks, 'write_to_database')
@patch.object(tasks, 'noti_handler')
@patch.object(log_schema, 'load')
@patch.object(tasks, 'check_log')
def test_init_task_matching_all(check_log, load, noti_handler,
                                write_to_database, log1, rule1,
                                rule2):
    load.return_value = Log(
        key_name='stress',
        value=50.5,
        host='From Marco',
        ip_address='123.132.213')
    tasks.init_task(log1)
    assert check_log.call_count > 1
    assert noti_handler.call_count > 1
    assert write_to_database.call_count > 1


@patch.object(tasks, 'write_to_database')
@patch.object(tasks, 'noti_handler')
@patch.object(log_schema, 'load')
@patch.object(tasks, 'check_log')
def test_init_task_not_matching(check_log, load, noti_handler,
                                write_to_database, log2, rule1):
    load.return_value = Log(
        key_name='nothing',
        value=155.5,
        host='malignant',
        ip_address='123.132.213')
    tasks.init_task(log2)
    check_log.assert_not_called()
    noti_handler.assert_not_called()
    write_to_database.assert_not_called()


def test_check_log_state_is_good(log1_model, rule2):
    assert tasks.check_log(log1_model, rule2) == 'Good'


def test_check_log_state_is_caution(log1_model, rule1):
    assert tasks.check_log(log1_model, rule1) == 'Caution'


def test_check_log_state_is_error(log2_model, rule1):
    assert tasks.check_log(log2_model, rule1) == 'Emergency'


@pytest.mark.parametrize("test_log, test_state", [
    (no_channel_log, 'Good'),
    (one_channel_log, 'Caution'),
    (multi_channel_log, 'Emergency')
])
@patch.object(Log, 'save')
@patch.object(Check, 'create')
@patch.object(db_session, 'commit')
def test_write_to_database_success(mock_db_session_commit,
                                   mock_check_create,
                                   mock_log_save,
                                   test_log, test_state, rule1):
    mock_log = log_schema.load(test_log)
    mock_log.id = 4444

    write_to_database(mock_log, rule1, test_state)

    mock_log_save.assert_called_with(only_flush=True)
    mock_check_create.assert_called_with(log_id=4444,
                                         rule_id=rule1.id,
                                         state=test_state,
                                         only_flush=True)
    mock_db_session_commit.assert_called_with()


@pytest.mark.parametrize("test_log, test_state", [
    (no_channel_log, 'Good'),
    (one_channel_log, 'Caution'),
    (multi_channel_log, 'Emergency')
])
@patch.object(Log, 'save')
@patch.object(Check, 'create')
@patch.object(db_session, 'commit')
def test_write_to_database_log_error(mock_db_session_commit,
                                     mock_check_create,
                                     mock_log_save,
                                     test_log, test_state, rule1):
    mock_log = log_schema.load(test_log)
    mock_log_save.side_effect = IntegrityError('this', 'is', 'test')

    with pytest.raises(IntegrityError):
        write_to_database(mock_log, rule1, test_state)

    mock_log_save.assert_called_with(only_flush=True)
    mock_check_create.assert_not_called()
    mock_db_session_commit.assert_not_called()


@patch('redis.StrictRedis.ttl', return_value=-1)
@patch('redis.StrictRedis.setex', return_value=None)
@patch.object(requests, 'post')
def test_noti_handler_no_channel(post, setex, ttl, no_channel_log_model, rule2):
    # input = log(dict), rule(model), state(string)
    # output = Post http request to WATUP API
    tasks.noti_handler(no_channel_log_model, rule2, 'Good')
    post.assert_not_called()


@patch('redis.StrictRedis.ttl', return_value=-1)
@patch('redis.StrictRedis.setex', return_value=None)
@patch.object(requests, 'post')
def test_noti_handler_one_channel(post, setex, ttl, one_channel_log_model,
                                  rule2):
    tasks.noti_handler(one_channel_log_model, rule2, 'Caution')
    post.assert_called_once()


@patch('redis.StrictRedis.ttl', return_value=-1)
@patch('redis.StrictRedis.setex', return_value=None)
@patch.object(requests, 'post')
def test_noti_handler_multi_channel(post, setex, ttl, multi_channel_log_model,
                                    rule2):
    tasks.noti_handler(multi_channel_log_model, rule2, 'Emergency')
    assert post.call_count > 1


@patch('redis.StrictRedis.ttl', return_value=-1)
@patch('redis.StrictRedis.setex', return_value=None)
@patch.object(requests, 'post')
def test_noti_handler_valid_cooltime(post, setex, ttl, one_channel_log_model,
                                     rule2):
    tasks.noti_handler(one_channel_log_model, rule2, 'Caution')
    post.assert_called_once()


@patch('redis.StrictRedis.ttl', return_value=5)
@patch('redis.StrictRedis.setex', return_value=None)
@patch.object(requests, 'post')
def test_noti_handler_invalid_cooltime(post, setex, ttl, one_channel_log_model,
                                       rule2):
    tasks.noti_handler(one_channel_log_model, rule2, 'Caution')
    post.assert_not_called()


@patch('redis.StrictRedis.ttl', return_value=-1)
@patch('redis.StrictRedis.setex', return_value=None)
@patch.object(requests, 'post')
def test_noti_handler_emergency_default_channel(post, setex, ttl,
                                                one_channel_log_model,
                                                empty_channel_rule):
    tasks.noti_handler(one_channel_log_model, empty_channel_rule, 'Emergency')
    post.assert_called_once()


@patch.object(requests, 'post')
def test_request_to_watup(mock_post):
    watup_url = 'https://watup.buzzni.net/v1/push/channels/'
    headers = {'Content-Type': 'application/json',
               'Authorization': 'Basic YnV6em5pOjY2ODg='}

    mock_response = tasks.request_to_watup('test_channel', 'test_body')

    mock_post.assert_called_once_with(watup_url + 'test_channel' + '/',
                                      headers=headers, json='test_body')
    assert mock_response == mock_post()


def test_get_rule_message(rule1):
    rule_message = tasks.get_rule_message(rule1)
    assert rule_message['name'] == rule1.name
    assert rule_message['description'] == rule1.description
    assert rule_message['owner'] == rule1.owner
    assert rule_message['email'] == rule1.email


@patch('redis.StrictRedis.setex', return_value=None)
@patch.object(Query, 'first')
@patch.object(tasks, 'request_to_watup')
@patch.object(tasks, 'datetime')
def test_check_timeout_exceeding_timeout_limit(mock_datetime,
                                               mock_request_to_watup,
                                               mock_query_first,
                                               mock_redis_setex,
                                               rule1):
    mock_datetime.utcnow.return_value = datetime(2018, 12, 11, 23, 50, 50)
    mock_query_first.return_value = \
        (None, Log(created_at=datetime(2018, 12, 11, 23, 50, 40)))

    tasks.check_timeout(rule1)
    mock_query_first.assert_called_with()

    calls = []
    for channel in json.loads(rule1.channel_timeout):
        calls.append(call(channel,
                          {'message': "[Timeout] {}의 {}".format(rule1.name,
                                                                rule1.key_name)}))

    mock_request_to_watup.assert_has_calls(calls)
    mock_redis_setex.assert_called_once()


@patch('redis.StrictRedis.setex', return_value=None)
@patch.object(Query, 'first')
@patch.object(tasks, 'request_to_watup')
@patch.object(tasks, 'datetime')
def test_check_timeout_within_timeout_limit(mock_datetime,
                                            mock_request_to_watup,
                                            mock_query_first,
                                            mock_redis_setex,
                                            rule1):
    mock_datetime.utcnow.return_value = datetime(2018, 12, 11, 23, 50, 49)
    mock_query_first.return_value = (
        None, Log(created_at=datetime(2018, 12, 11, 23, 50, 40)))

    tasks.check_timeout(rule1)
    mock_request_to_watup.assert_not_called()
    mock_redis_setex.assert_not_called()


@patch('redis.StrictRedis.ttl', return_value=-1)
@patch.object(Rule, 'read')
@patch.object(tasks, 'check_timeout')
def test_periodic_timeout_checker_valid_timeout_attribute(mock_check_timeout,
                                                          mock_rule_read,
                                                          mock_redis_ttl,
                                                          valid_timeout_rule):
    mock_rule_read.return_value = [valid_timeout_rule]
    tasks.periodic_timeout_checker()
    mock_rule_read.assert_called_once_with()
    mock_redis_ttl.assert_called_once()
    mock_check_timeout.assert_has_calls([call(valid_timeout_rule)])


@patch('redis.StrictRedis.ttl', return_value=-1)
@patch.object(Rule, 'read')
@patch.object(tasks, 'check_timeout')
def test_periodic_timeout_checker_empty_timeout_attribute(mock_check_timeout,
                                                          mock_rule_read,
                                                          mock_redis_ttl,
                                                          empty_timeout_rule):
    mock_rule_read.return_value = [empty_timeout_rule]
    tasks.periodic_timeout_checker()
    mock_rule_read.assert_called_once_with()
    mock_redis_ttl.assert_not_called()
    mock_check_timeout.assert_not_called()


@patch('redis.StrictRedis.ttl', return_value=60)
@patch.object(Rule, 'read')
@patch.object(tasks, 'check_timeout')
def test_periodic_timeout_checker_wating_cooltime(mock_check_timeout,
                                                  mock_rule_read,
                                                  mock_redis_ttl,
                                                  valid_timeout_rule):
    mock_rule_read.return_value = [valid_timeout_rule]
    tasks.periodic_timeout_checker()
    mock_rule_read.assert_called_once_with()
    mock_redis_ttl.assert_called_once()
    mock_check_timeout.assert_not_called()


@patch('redis.StrictRedis.ttl', return_value=-1)
@patch.object(Rule, 'read')
@patch.object(tasks, 'check_timeout')
def test_periodic_timeout_checker_passing_cooltime(mock_check_timeout,
                                                   mock_rule_read,
                                                   mock_redis_ttl,
                                                   valid_timeout_rule):
    mock_rule_read.return_value = [valid_timeout_rule]
    tasks.periodic_timeout_checker()
    mock_rule_read.assert_called_once_with()
    mock_redis_ttl.assert_called_once()
    mock_check_timeout.assert_has_calls([call(valid_timeout_rule)])


def test_get_message_for_json(log, rule):
    state = "Good"
    channel = "1"
    message = get_message(log, rule, state, channel, detail=False)

    json_message = json.loads(message)
    # Agent
    assert json_message["agent"]["host"] == log.host
    assert json_message["agent"]["ip_address"] == log.ip_address
    # Log
    assert json_message["log"]["key_name"] == log.key_name
    assert json_message["log"]["value"] == log.value
    assert json_message["log"]["created_at"] == log.created_at.strftime(
        "%Y-%m-%d %H:%M:%S")
    assert json_message["log"]["description"] == log.description
    # Rule
    assert json_message["rule"]["name"] == rule.name
    assert json_message["rule"]["description"] == rule.description
    assert json_message["rule"]["owner"] == rule.owner
    assert json_message["rule"]["email"] == rule.email

    # Level
    assert json_message["level"] == state


def test_get_message_for_noti(log, rule):
    state = "Good"
    channel = "1"
    noti_message = get_message(log, rule, state, channel, detail=True)

    assert noti_message == "[" + state + "] " + log.key_name + ": " + str(
        log.value) + ", [" + rule.description + "] [" + (
                   log.description or "") + "]"
