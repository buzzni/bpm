from unittest.mock import patch, MagicMock

from flask import json
from flask_api import status

from bpm.database import db_session
from bpm.models import Check
from bpm.schemas import rule_schema

CHECKS_API_URL = '/api/v1/checks/'


class TestChecksApi(object):
    @patch.object(rule_schema, 'dump')
    @patch.object(Check, 'count')
    @patch.object(Check, 'read')
    def test_checks_get_all_empty(self, check_read, check_count,
                                  rule_schema_dump, flask_client):
        check_read.return_value = []
        check_count.return_value = 0
        rule_schema_dump.return_value = "[]"
        response = flask_client.get(CHECKS_API_URL)

        response_json = json.loads(response.get_data(as_text=True))

        assert response_json["total"] == 0

        assert response.status_code == status.HTTP_200_OK

    @patch.object(db_session, 'query')
    def test_checks_get_all(self, mock_query, flask_client):
        mock_query().join().filter().offset().limit.return_value = []
        mock_query().join().filter().count.return_value = 0

        response = flask_client.get(CHECKS_API_URL + "?key_name=daily")

        response_json = json.loads(response.get_data(as_text=True))
        mock_query.assert_called()
        mock_query().join().filter.assert_called()
        assert response_json["total"] == 0

        assert response.status_code == status.HTTP_200_OK
