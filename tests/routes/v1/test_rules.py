import json
from unittest.mock import patch, MagicMock

from flask_api import status
from marshmallow import ValidationError
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from bpm.models.rule import Rule
from bpm.schemas import rule_schema
from tests.dummy_rule import valid_rule, reverse_boundary_rule, \
    missing_key_name_field_rule

RULE_API_URL = '/api/v1/rules/'


@patch.object(Rule, 'read_one')
def test_rule_get_success(mock_read_one, flask_client, rule):
    # return one rule
    rule.id = 100
    mock_read_one.return_value = rule

    response = flask_client.get(RULE_API_URL + str(rule.id))
    response_json = json.loads(response.get_data(as_text=True))

    mock_read_one.assert_called_with(id=rule.id)
    assert response.status_code == status.HTTP_200_OK
    assert response_json['email'] == rule.email
    assert response_json['description'] == rule.description
    assert response_json['name'] == rule.name
    assert response_json['owner'] == rule.owner


@patch.object(Rule, 'read_one')
def test_rule_get_no_result_found(mock_read_one, flask_client, rule):
    mock_read_one.side_effect = NoResultFound
    response = flask_client.get(RULE_API_URL + str(rule.id))
    assert response.status_code == status.HTTP_404_NOT_FOUND


@patch.object(rule_schema, 'dump')
@patch.object(Rule, 'read')
def test_rule_list_get(mock_read, rule_schema_dump, flask_client, rule):
    # return list
    mock_read.return_value = rule
    rule_schema_dump.return_value = [{
        "email": rule.email,
        "description": rule.description,
        "name": rule.name,
        "owner": rule.owner
    }]
    response = flask_client.get(RULE_API_URL)

    assert response.status_code == status.HTTP_200_OK

    response_json = json.loads(response.get_data(as_text=True))["list"][0]

    assert rule.email == response_json['email']
    assert rule.description == response_json['description']
    assert rule.name == response_json['name']
    assert rule.owner == response_json['owner']


@patch.object(Rule, 'save')
def test_rule_post(mock_create, flask_client):
    # No data
    response = flask_client.post(RULE_API_URL)
    assert response.status_code == status.HTTP_400_BAD_REQUEST

    # First rule add
    response = flask_client.post(RULE_API_URL, data=json.dumps(valid_rule),
                                 content_type='application/json')
    assert response.status_code == status.HTTP_201_CREATED
    response_json = json.loads(response.get_data(as_text=True))
    assert "id" in response_json
    assert valid_rule['email'] == response_json['email']
    assert valid_rule['channels'] == response_json['channels']
    assert valid_rule['name'] == response_json['name']
    assert valid_rule['owner'] == response_json['owner']
    assert valid_rule['timeout'] == response_json['timeout']

    # Add same rule
    mock_create.side_effect = IntegrityError('this',
                                             'is',
                                             'test')
    response = flask_client.post(RULE_API_URL, data=json.dumps(valid_rule),
                                 content_type='application/json')
    assert response.status_code == status.HTTP_409_CONFLICT


@patch.object(rule_schema, 'load')
def test_rule_post_reverse_boundary_rule(rule_schema_load, flask_client):
    #  post Invalid rule:  boundary reverse
    rule_schema_load.side_effect = ValidationError("")
    response = flask_client.post(RULE_API_URL,
                                 data=json.dumps(reverse_boundary_rule),
                                 content_type='application/json')
    rule_schema_load.assert_called_once()

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@patch.object(rule_schema, 'load')
def test_rule_post_missing_key_name_rule(rule_schema_load, flask_client):
    rule_schema_load.side_effect = ValidationError("")

    # post Invalid rule: missing field
    response = flask_client.post(RULE_API_URL,
                                 data=json.dumps(missing_key_name_field_rule),
                                 content_type='application/json')
    rule_schema_load.assert_called_once()

    assert response.status_code == status.HTTP_400_BAD_REQUEST


@patch.object(rule_schema, 'dump')
@patch.object(Rule, 'read_one')
def test_rule_patch_success(rule_read_one, rule_schema_dump, flask_client):
    args = {
        "key_name": "qwer"
    }

    mock_rule_read_one_result = MagicMock()
    rule_read_one.return_value = mock_rule_read_one_result
    rule_schema_dump.return_value = {
        "name": "test_rule",
        "description": "rule for testing",
        "owner": "Marco",
        "email": "marco@buzzni.com",
        "key_name": "qwer",
        "rule": {
            "type": "A",
            "boundary1": 30,
            "boundary2": 50
        },
        "channels": {
            "good": ["1", "2"],
            "caution": ["1", "2"],
            "emergency": ["1", "2"]
        }
    }
    response = flask_client.patch(RULE_API_URL + "1", data=json.dumps(args),
                                  content_type='application/json')

    rule_read_one.assert_called_once_with(id=1)

    mock_rule_read_one_result.update.assert_called_once()
    rule_schema_dump.assert_called_once_with(mock_rule_read_one_result)

    assert response.status_code == status.HTTP_200_OK


@patch.object(rule_schema, 'to_model_json')
@patch.object(Rule, 'read_one')
def test_rule_patch_invalid(rule_read_one, to_model_json, flask_client):
    args = {
        "asdf": "qwer"
    }
    mock_rule_read_one_result = MagicMock()
    rule_read_one.return_value = mock_rule_read_one_result

    to_model_json.side_effect = AttributeError()
    response = flask_client.patch(RULE_API_URL + "1", data=json.dumps(args),
                                  content_type='application/json')
    rule_read_one.assert_called_once_with(id=1)
    mock_rule_read_one_result.update.assert_not_called()
    assert response.status_code == status.HTTP_400_BAD_REQUEST


@patch.object(Rule, 'update')
def test_rule_patch_not_found(rule_update, flask_client):
    args = {
        "key_name": "zxcv"
    }
    response = flask_client.patch(RULE_API_URL + "999", data=json.dumps(args))

    rule_update.assert_not_called()

    assert response.status_code == status.HTTP_404_NOT_FOUND


@patch.object(Rule, 'delete')
def test_rule_delete_invalid_url(mock_delete, flask_client, rule):
    response = flask_client.delete(RULE_API_URL)
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


@patch.object(Rule, 'read_one')
def test_rule_delete_id_not_found(rule_read_one, flask_client, rule):
    rule_read_one.side_effect = NoResultFound
    response = flask_client.delete(RULE_API_URL + str(rule.id + 999))
    rule_read_one.delete.assert_not_called()
    assert response.status_code == status.HTTP_404_NOT_FOUND


@patch.object(Rule, 'read_one')
def test_rule_delete(rule_read_one, flask_client, rule):
    rule_one = MagicMock()
    rule_read_one.return_value = rule_one
    response = flask_client.delete(RULE_API_URL + str(rule.id))

    rule_one.delete.assert_called_once()

    assert response.status_code == status.HTTP_204_NO_CONTENT
