import json
from unittest.mock import patch

from flask_api import status
from marshmallow import ValidationError

from bpm import tasks
from bpm.models.log import Log
from bpm.models.rule import Rule
from bpm.schemas import rule_schema, log_schema
from tests.conftest import log1
from tests.dummy_log import string_value_log_from_agent
from tests.dummy_rule import valid_rule, valid_rule2

LOG_API_URL = '/api/v1/demo/'

demo_json_success = {
    "log": {
        "key_name": "stress",
        "value": 50,
        "host": "From Marco",
        "ip_address": "123.132.213"
    },
    "rule": {
        "name": "test_rule",
        "description": "rule for testing",
        "owner": "Marco",
        "email": "marco@buzzni.com",
        "key_name": "stress",
        "timeout": 10,
        "rule": {
            "type": "A",
            "boundary1": 30,
            "boundary2": 50
        },
        "channels": {
            "good": ["1", "2"],
            "caution": ["1", "2"],
            "emergency": ["1", "2"],
            "timeout": ["1"]
        }
    }
}
demo_json_rule_error = {
    "log": {
        "key_name": "stress",
        "value": 50,
        "host": "From Marco",
        "ip_address": "123.132.213"
    },
    "rule": {
        "name": "test_invalid_rule1",
        "description": "rule for testing",
        "owner": "Dean",
        "email": "marco@buzzni.com",
        "key_name": "stress",
        "timeout": 10,

        "rule": {
            "type": "A",
            "boundary1": 60,
            "boundary2": 20
        },
        "channels": {
            "good": ["1", "2"],
            "caution": ["1", "2"],
            "emergency": ["1", "2"],
            "timeout": ["1"]
        }
    }
}
demo_json_log_error = {
    "log": string_value_log_from_agent,
    "rule": valid_rule
}
demo_json_key_not_match = {
    "log": log1,
    "rule": valid_rule2,
}

DEMO_API_URL = '/api/v1/demo'


class TestDemoApi(object):
    # POST method

    @patch.object(tasks, 'check_log')
    @patch.object(log_schema, 'load')
    @patch.object(rule_schema, 'load')
    def test_demo_post_success(self, rule_load, log_load, check_log,
                               flask_client):
        rule_load.return_value = Rule(name='test_rule2',
                                      description='Rule 2 Test',
                                      owner='Dean',
                                      email='Dean@buzzni.com',
                                      key_name='stress',
                                      type='A',
                                      boundary1=90,
                                      boundary2=95,
                                      channel_good='[]',
                                      channel_caution='["channel_1"]',
                                      channel_emergency='["channel_1", "channel_2"]')

        log_load.return_value = Log(
            key_name='stress',
            value=93,
            host='From Marco',
            ip_address='123.132.213')
        check_log.return_value = 'Caution'
        response = flask_client.post(DEMO_API_URL,
                                     data=json.dumps(demo_json_success),
                                     content_type='application/json')
        rule_load.assert_called_once()
        log_load.assert_called_once()
        check_log.assert_called_once_with(log_load.return_value,
                                          rule_load.return_value)

        response_json = json.loads(response.get_data(as_text=True))

        assert response_json == "Caution"
        assert response.status_code == status.HTTP_200_OK

    @patch.object(tasks, 'check_log')
    @patch.object(log_schema, 'load')
    @patch.object(rule_schema, 'load')
    def test_demo_post_rule_error(self, rule_load, log_load, check_log,
                                  flask_client):
        rule_load.return_value = Rule(name='test_rule2',
                                      description='Rule 2 Test',
                                      owner='Dean',
                                      email='Dean@buzzni.com',
                                      key_name='stress',
                                      type='A',
                                      boundary1=95,
                                      boundary2=80,
                                      channel_good='[]',
                                      channel_caution='["channel_1"]',
                                      channel_emergency='["channel_1", "channel_2"]')

        log_load.return_value = Log(
            key_name='stress',
            value=50,
            host='From Marco',
            ip_address='123.132.213')
        rule_load.side_effect = ValidationError({'_schema': [
            'boundary2 must be greater or equal to boundary1 in Type A']})

        response = flask_client.post(DEMO_API_URL,
                                     data=json.dumps(demo_json_rule_error),
                                     content_type='application/json')

        log_load.assert_called_once()
        check_log.assert_not_called()

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @patch.object(tasks, 'check_log')
    @patch.object(log_schema, 'load')
    @patch.object(rule_schema, 'load')
    def test_demo_post_log_error(self, rule_load, log_load, check_log,
                                 flask_client):
        rule_load.return_value = Rule(name='test_rule2',
                                      description='Rule 2 Test',
                                      owner='Dean',
                                      email='Dean@buzzni.com',
                                      key_name='stress',
                                      type='A',
                                      boundary1=95,
                                      boundary2=80,
                                      channel_good='[]',
                                      channel_caution='["channel_1"]',
                                      channel_emergency='["channel_1", "channel_2"]')

        log_load.return_value = Log(
            key_name='stress',
            value=50,
            host='From Marco',
        )
        log_load.side_effect = ValidationError(
            {'ip_address': ['Missing data for required field.'],
             '_schema': ['required field is not exists']})

        response = flask_client.post(DEMO_API_URL,
                                     data=json.dumps(demo_json_log_error),
                                     content_type='application/json')
        log_load.assert_called_once()
        check_log.assert_not_called()

        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @patch.object(tasks, 'check_log')
    @patch.object(log_schema, 'load')
    @patch.object(rule_schema, 'load')
    def test_demo_post_key_not_match(self, rule_load, log_load, check_log,
                                     flask_client):
        rule_load.return_value = Rule(name='test_rule2',
                                      description='Rule 2 Test',
                                      owner='Dean',
                                      email='Dean@buzzni.com',
                                      key_name='happy',
                                      type='A',
                                      boundary1=95,
                                      boundary2=80,
                                      channel_good='[]',
                                      channel_caution='["channel_1"]',
                                      channel_emergency='["channel_1", "channel_2"]')

        log_load.return_value = Log(
            key_name='stress',
            value=50,
            host='From Marco',
            ip_address='123.132.213')

        response = flask_client.post(DEMO_API_URL,
                                     data=json.dumps(demo_json_log_error),
                                     content_type='application/json')
        rule_load.assert_called_once()
        log_load.assert_called_once()
        assert rule_load.return_value.key_name != log_load.return_value.key_name
        check_log.assert_not_called()

        response_json = json.loads(response.get_data(as_text=True))
        assert "key not match" == response_json
        assert response.status_code == status.HTTP_400_BAD_REQUEST
