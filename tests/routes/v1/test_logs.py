import json
from unittest.mock import patch

from flask_api import status

from bpm.database import db_session
from bpm.schemas import log_schema
from tests.dummy_log import string_value_log_from_agent, valid_log_from_agent, \
    missing_field_log_from_agent

LOG_API_URL = '/api/v1/logs/'


class TestLogsApi(object):
    # POST method

    @patch('bpm.tasks.init_task.delay')
    @patch.object(log_schema, 'validate')
    def test_log_post_success(self, log_validate, delay, flask_client):
        log_validate.return_value = {}
        response = flask_client.post(LOG_API_URL,
                                     data=json.dumps(valid_log_from_agent),
                                     content_type='application/json')
        delay.assert_called_once()
        assert response.status_code == status.HTTP_202_ACCEPTED

    @patch('bpm.tasks.init_task.delay')
    @patch.object(log_schema, 'validate')
    def test_log_post_missing_field(self, log_validate, delay, flask_client):
        log_validate.return_value = {'asdf': 'some error'}
        response = flask_client.post(LOG_API_URL, data=json.dumps(
            missing_field_log_from_agent),
                                     content_type='application/json')

        delay.assert_not_called()
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @patch('bpm.tasks.init_task.delay')
    @patch.object(log_schema, 'validate')
    def test_log_post_string_value(self, log_validation, delay, flask_client):
        log_validation.return_value = {'asdf': 'some error'}
        response = flask_client.post(LOG_API_URL, data=json.dumps(
            string_value_log_from_agent),
                                     content_type='application/json')

        delay.assert_not_called()
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    @patch('bpm.tasks.init_task.delay')
    def test_log_post_no_data(self, delay, flask_client):
        response = flask_client.post(LOG_API_URL)
        delay.assert_not_called()
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    # GET Method
    @patch.object(db_session, 'query')
    def test_log_get_with_distinct(self, mock_query, flask_client):
        response = flask_client.get(LOG_API_URL + "?distinct=key_name")
        mock_query.assert_called_once()
        mock_query().distinct.assert_called_once()
        assert response.status_code == status.HTTP_200_OK

    def test_log_get_noargs(self, flask_client):
        response = flask_client.get(LOG_API_URL)
        assert response.status_code == status.HTTP_400_BAD_REQUEST

    # PATCH method
    def test_log_patch(self, flask_client):
        response = flask_client.patch(LOG_API_URL)
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED

    # DELETE method
    def test_log_delete(self, flask_client):
        response = flask_client.patch(LOG_API_URL)
        assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED
