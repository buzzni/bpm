from bpm.app import create_app

# def run_celery():
#     w = worker.worker(app=celery_app)
#     w.run(beat=True, concurrency=1)

if __name__ == '__main__':
    # if len(sys.argv) > 1:
    #     if sys.argv[1] == 'celery':
    #         run_celery()

        # elif sys.argv[1] == 'both':
        #     p = Process(target=run_celery)
        #     p.start()
        #     app = create_app()
        #     app.run(host='0.0.0.0', debug=False)
    # else:
    app = create_app()
    app.run(host='0.0.0.0', debug=False)
